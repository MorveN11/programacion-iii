package progamacion.iii.tasks;

public class Algorithms {
  private Algorithms() {

  }

  public static int algorithmN3(int[] arr) {
    int n = arr.length;
    int best = 0;
    for (int a = 0; a < n; a++) {
      for (int b = 0; b < n; b++) {
        int sum = 0;
        for (int k = a; k <= b; k++) {
          sum += arr[k];
        }
        best = Math.max(best, sum);
      }
    }
    return best;
  }

  public static int algorithmN2(int[] arr) {
    int n = arr.length;
    int best = 0;
    for (int a = 0; a < n; a++) {
      int sum = 0;
      for (int b = a; b < n; b++) {
        sum += arr[b];
        best = Math.max(best, sum);
      }
    }
    return best;
  }

  public static int algorithmN(int[] arr) {
    int n = arr.length;
    int best = 0;
    int sum = 0;
    for (int k = 0; k < n; k++) {
      sum = Math.max(arr[k], sum + arr[k]);
      best = Math.max(best, sum);
    }
    return best;
  }
}
