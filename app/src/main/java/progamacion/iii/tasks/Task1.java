package progamacion.iii.tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Task1 {
  private Task1() {

  }

  /**
   * This method generate a Pascal.
   * 
   * @param size The size of the Pascal.
   * @return
   */
  public static List<List<Integer>> generatePascal(int size) {
    if (size < 0) {
      throw new IndexOutOfBoundsException();
    }
    if (size == 0) {
      return new ArrayList<>();
    }
    return generatePascal(size, new ArrayList<>(List.of(List.of(1))));
  }

  /**
   * This method generate a Pascal.
   * 
   * @param size   The size of the Pascal.
   * @param pascal The pascal to generate.
   * @return
   */
  private static List<List<Integer>> generatePascal(int size, List<List<Integer>> pascal) {
    if (size == 1) {
      return pascal;
    }
    pascal.add(Stream.of(
        List.of(1),
        IntStream.range(0, pascal.get(pascal.size() - 1).size() - 1)
            .map(i -> pascal.get(pascal.size() - 1).get(i)
                + pascal.get(pascal.size() - 1).get(i + 1))
            .boxed()
            .collect(Collectors.toList()),
        List.of(1))
        .flatMap(n -> n.stream())
        .collect(Collectors.toList()));
    return generatePascal(size - 1, pascal);
  }
}
