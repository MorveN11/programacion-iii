package progamacion.iii.tasks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class Task1Test {
  /**
   * This method test generate a Pascal of 6 levels.
   */
  @Test
  void testPascalOf6Levels() {
    assertEquals(new ArrayList<>(
        List.of(
            List.of(1),
            List.of(1, 1),
            List.of(1, 2, 1),
            List.of(1, 3, 3, 1),
            List.of(1, 4, 6, 4, 1),
            List.of(1, 5, 10, 10, 5, 1))),
        Task1.generatePascal(6));
  }

  @Test
  void testPascalOfNegativeLevels() {
    assertThrows(IndexOutOfBoundsException.class, () -> Task1.generatePascal(-4));
  }

  @Test
  void testPascalOf0Levels() {
    assertEquals(new ArrayList<>(), Task1.generatePascal(0));
  }
}
