package progamacion.iii.tasks;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Random;

import org.junit.jupiter.api.Test;

class AlgorithmsTest {
  @Test
  void testAlgorithmN3() {

    Random rnd = new Random();
    int[] arr = new int[1000];
    for (int i = 0; i < arr.length; i++) {
      arr[i] = rnd.nextInt();
    }

    double before = System.nanoTime();
    int result = Algorithms.algorithmN3(arr);
    double elapsedTime = (System.nanoTime() - before) / 1000000;
    System.out.println("1. Case N^3 Algorithm");
    System.out.println("Total Execution in millis: " + elapsedTime + "\n");

    double before2 = System.nanoTime();
    int result2 = Algorithms.algorithmN2(arr);
    double elapsedTime2 = (System.nanoTime() - before2) / 1000000;
    System.out.println("2. Case N^2 Algorithm");
    System.out.println("Total Execution in millis: " + elapsedTime2 + "\n");
    assertEquals(result, result2);

    double before3 = System.nanoTime();
    Algorithms.algorithmN(arr);
    double elapsedTime3 = (System.nanoTime() - before3) / 1000000;
    System.out.println("3. Case N Algorithm");
    System.out.println("Total Execution in millis: " + elapsedTime3 + "\n");

    System.out.println("Result: " + result);
  }
}
